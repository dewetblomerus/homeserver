# Home Server with everything except Samba starting with Doccker Compose

### Setup

1. Install Docker.
1. Install Docker Compose.
1. `cp sample.env .env` and edit the file with your secrets.
1. `cp sample.ddclient.conf ddclient.conf` and edit the file with your secrets.
1. Install Samba, this worked well last time I did it https://www.raspberrypi.org/magpi/samba-file-server/

### Links
https://engineerworkshop.com/blog/raspberry-pi-vlan-how-to-connect-your-rpi-to-multiple-networks/

### Nextcloud

Config file:
`~/config/nextcloud/config/www/nextcloud/config/config.php`

### Swag

Config file:
`~/config/swag/nginx/site-confs/default`
